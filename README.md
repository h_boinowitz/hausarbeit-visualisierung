# Hausarbeit Visualisierung

Hausarbeit im Rahmen der Lehrveranstaltung "Visualisierung" an der TU Braunschweig im WiSe 20/21.

## Zielsetzung
Dieser Bericht soll anhand eines Vortrages zur historischen Bedeutung des Foucaultschen Pendels den Prozess der Erstellung einer Präsentation und thematisch passender Bilder dokumentieren.

Im Rahmen der Lehrveranstaltung "Visualisierung" wurden dazu verschiedene technische Lösungen erläutert. Dabei stehen die Textverarbeitung mit LaTeX und die Gestaltung von Plots mit Python, hier in Form eines Jupyter-Notebooks umgesetzt, im Vordergrund.

Es sollen die in naturwissenschaftlichen Publikationen üblichen Darstellungsformate — 2D-, 3D-Plot und Animation — erprobt werden. Der Prozess der Erstellung und die dabei getroffenen gestalterischen Entscheidungen sollen dabei in einem Bericht festgehalten werden.

## Ordnerstruktur
- Der Source-Code für die Plots als Jupyter-Notebook und die LaTeX-Files für die Präsentation und den zugehörigen Bericht finden sich im Ordner _Code_.
- Die PDFs der Präsenation und des Berichts können jeweils in den gleichnamigen Ordnern abgerufen werden.
- Im Ordner _Grafiken_ sind alle mit Python gernerierten Plots als hochauflösende PGN-Files abrufbar.
- Im Ordner _Animation_ findet sich das MP4-File der Animation der Schwingung eines Foucaultschen Pendels

